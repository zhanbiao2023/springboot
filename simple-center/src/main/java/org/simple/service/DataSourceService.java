package org.simple.service;


import com.baomidou.mybatisplus.extension.service.IService;
import org.simple.entity.DataSource;

public interface DataSourceService extends IService<DataSource> {
}
