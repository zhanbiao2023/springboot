package org.simple.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.simple.entity.Email;

/**
 * EmailService
 *
 * @author frsimple
 * @version v1.0
 * @since 2022/11/13
 */
public interface IEmailService extends IService<Email> {
}
