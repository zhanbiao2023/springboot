package org.simple.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.simple.entity.DataSource;

public interface DataSourceMapper extends BaseMapper<DataSource> {
}
