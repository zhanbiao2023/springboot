package org.simple.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.simple.entity.TableCfg;

public interface TableCfgMapper extends BaseMapper<TableCfg> {

}
